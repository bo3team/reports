package com.progressoft.reports.feeder;

import java.util.Objects;

class DefaultNestedStructure implements NestedStructure {

    private final String fieldName;
    private final ClassStructure classStructure;
    private final FetchStrategy fetchStrategy;

    DefaultNestedStructure(String fieldName, ClassStructure classStructure) {
        this.fieldName = fieldName;
        this.classStructure = classStructure;
        this.fetchStrategy = FetchStrategy.collectionFetchStrategy;
    }

    DefaultNestedStructure(String fieldName, ClassStructure classStructure, FetchStrategy fetchStrategy) {
        this.fieldName = fieldName;
        this.classStructure = classStructure;
        this.fetchStrategy = fetchStrategy;
    }

    @Override
    public void findNestedCollections(final ClassStructure.ClassStructureListener classStructureListener, final Object rootObject) {
        processNestedCollection(classStructureListener, new ReflectionValueExtractor().extractValue(rootObject, fieldName));
    }

    private void processNestedCollection(final ClassStructure.ClassStructureListener classStructureListener, final Object nestedValue) {
        if (isEmptyNestedCollection(nestedValue))
            classStructureListener.onNewCollectionFound(fetchStrategy.getNestedObjectsAsCollection(nestedValue), classStructure);
    }

    private boolean isEmptyNestedCollection(Object nestedValue) {
        return Objects.nonNull(nestedValue) && nestedListNotEmpty(nestedValue);
    }

    private boolean nestedListNotEmpty(Object nestedValue) {
        return !fetchStrategy.getNestedObjectsAsCollection(nestedValue).isEmpty();
    }
}
