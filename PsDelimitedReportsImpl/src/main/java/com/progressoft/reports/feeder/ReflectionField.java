package com.progressoft.reports.feeder;

import com.progressoft.reports.feeder.formatter.ReflectionFormatter;
import com.progressoft.reports.feeder.formatter.ValueFormatter;

public class ReflectionField implements ReflectionFormatter {

    private final String fieldName;
    private final ValueFormatter formatter;

    public ReflectionField(String fieldName, ValueFormatter formatter) {
        this.fieldName = fieldName;
        this.formatter = formatter;
    }

    @Override
    public String getFormattedValueFromObject(final Object rootObject) {
        return formatter.formatFieldValue(getFieldValue(rootObject));
    }

    private Object getFieldValue(final Object rootObject) {
        return new ReflectionValueExtractor().extractValue(rootObject, fieldName);
    }
}
