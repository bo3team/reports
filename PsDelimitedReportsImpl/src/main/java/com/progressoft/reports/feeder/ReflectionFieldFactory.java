package com.progressoft.reports.feeder;

import com.progressoft.reports.RecordFactory;
import com.progressoft.reports.feeder.formatter.ValueFormatter;

public interface ReflectionFieldFactory {

    ReflectionField makeBigDecimalField(final String fieldName);

    ReflectionField makeStringField(final String fieldName);

    ReflectionField makeNumberField(final String fieldName);

    ReflectionField makeNumberField(final String fieldName, final Number defaultValue);

    ReflectionField makeNumberField(final String fieldName, final String defaultValue);

    ReflectionField makeDateField(final String fieldName);

    ReflectionField makeDateField(final String fieldName, final String pattern);

    ReflectionField makeObjectField(final String fieldName);

    ReflectionField makeObjectField(final String fieldName, final ValueFormatter formatter);

    ReflectionField makeCollectionField(final String fieldName, final String delimiter, final ValueFormatter formatter);

    ReflectionField makeCollectionField(final String fieldName, final String delimiter, final ValueFormatter formatter, final RecordFactory recordFactory);
}
