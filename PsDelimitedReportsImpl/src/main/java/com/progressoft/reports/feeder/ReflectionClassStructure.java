package com.progressoft.reports.feeder;

import com.progressoft.reports.FormattableValue;
import com.progressoft.reports.Record;
import com.progressoft.reports.RecordFactory;
import com.progressoft.reports.StringValue;
import com.progressoft.reports.feeder.formatter.ReflectionFormatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ReflectionClassStructure implements ClassStructure {

    private final List<ReflectionFormatter> fields = new ArrayList<>();
    private final String delimiter;
    private final String discriminator;
    private final List<NestedStructure> nestedStructures = new ArrayList<>();
    private final RecordFactory recordFactory;

    ReflectionClassStructure(String delimiter, String discriminator, RecordFactory recordFactory) {
        this.delimiter = delimiter;
        this.discriminator = discriminator;
        this.recordFactory = recordFactory;
    }


    @Override
    public void addField(final ReflectionFormatter field) {
        fields.add(field);
    }

    @Override
    public void addFields(final Collection<ReflectionFormatter> fields) {
        this.fields.addAll(fields);
    }

    @Override
    public void addFields(final ReflectionFormatter... fields) {
        Collections.addAll(this.fields, fields);
    }

    @Override
    public Record createRecord(final Object rootObject) {
        return addValues(rootObject, createNewRecord());
    }

    private Record addValues(Object rootObject, Record r) {
        fields.stream().forEach(f -> r.addValue(convertPropertyToRecordValue(rootObject, f)));
        return r;
    }

    private Record createNewRecord() {
        return RecordFactory.INSTANCE.makeDiscriminatedRecord(this.delimiter, this.discriminator);
    }

    private FormattableValue convertPropertyToRecordValue(final Object rootObject, final ReflectionFormatter field) {
        return new StringValue(field.getFormattedValueFromObject(rootObject));
    }

    @Override
    public void addNestedStructure(NestedStructure nestedStructure) {
        this.nestedStructures.add(0, nestedStructure);
    }

    public void nestedClassStructures(ClassStructureListener classStructureListener, Object rootObject) {
        nestedStructures.stream().forEach(n -> n.findNestedCollections(classStructureListener, rootObject));
    }
}
