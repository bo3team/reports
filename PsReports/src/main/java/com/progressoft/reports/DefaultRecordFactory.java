package com.progressoft.reports;

import java.util.Optional;

public class DefaultRecordFactory implements RecordFactory {

    @Override
    public Record makeCsvRecord() {
        return new DefaultRecord();
    }

    @Override
    public Record makeDelimitedRecord(final String delimiter) {
        return new DefaultRecord(delimiter);
    }

    @Override
    public Record makeDiscriminatedRecord(String delimiter, String discriminator) {
        return new DefaultRecord(delimiter, Optional.ofNullable(discriminator));
    }
}
