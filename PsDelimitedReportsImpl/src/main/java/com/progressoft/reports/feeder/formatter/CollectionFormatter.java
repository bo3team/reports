package com.progressoft.reports.feeder.formatter;

import com.progressoft.reports.Record;
import com.progressoft.reports.RecordFactory;

import java.util.Collection;
import java.util.Objects;

public class CollectionFormatter implements ValueFormatter {

    public static final String EMPTY_STRING = "";
    private final ValueFormatter formatter;
    private final String delimiter;
    private final RecordFactory recordFactory;

    public CollectionFormatter(String delimiter, ValueFormatter formatter) {
        this.formatter = formatter;
        this.delimiter = delimiter;
        this.recordFactory = RecordFactory.INSTANCE;
    }

    public CollectionFormatter(String delimiter, ValueFormatter formatter, RecordFactory recordFactory) {
        this.formatter = formatter;
        this.delimiter = delimiter;
        this.recordFactory = recordFactory;
    }

    @Override
    public String formatFieldValue(final Object value) {
        return Objects.isNull(value) ? EMPTY_STRING : formatCollection((Collection) value);
    }

    private String formatCollection(final Collection c) {
        Record r = recordFactory.makeDelimitedRecord(delimiter);
        c.stream().forEach(o -> r.addValue(formatter.formatFieldValue(o)));
        return r.toLine();
    }
}
