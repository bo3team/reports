package com.progressoft.reports;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class DelimitedValueReportTest {

    private class CollectionRecordsFeeder implements RecordsFeeder {
        private final Iterator<Record> iterator;

        CollectionRecordsFeeder(Collection<Record> records) {
            this.iterator=records.iterator();
        }

        public boolean hasNext() {
            return iterator.hasNext();
        }

        public Record next() {
            return iterator.next();
        }
    }

    private class StringReportWriter implements ReportWriter {
        private String lines = "";

        String result() {
            return lines;
        }

        public void writeLine(String line) {
            lines += line + lineSeparator();
        }

        public void writeLastLine(String line) {
            lines += line;
        }

        private String lineSeparator() {
            return System.getProperty("line.separator");
        }
    }

    private static final String SIMPLE_DATE_FORMAT = "dd/MM/yyyy";
    private static final String VALUE_1 = "value1";
    private static final String VALUE_2 = "value2";
    private static final String COMMA = ",";
    private static final String EMPTY_STRING = "";
    private static final String BAR = "|";

    private Record record1;
    private Record record2;
    private Record barDelimitedRecord;

    private RecordsFeeder recordsFeeder;
    private StringReportWriter reportWriter;
    private DelimitedValueReport emptyReport;

    @Before
    public void setUp() throws Exception {
        reportWriter = new StringReportWriter();

        List<Record> recordsList = new ArrayList<Record>() {{
            this.add(record1);
            this.add(record2);
        }};
        recordsFeeder = new CollectionRecordsFeeder(recordsList);
        CollectionRecordsFeeder emptyRecordsGateway = new CollectionRecordsFeeder(new ArrayList<>());

        record1 = RecordFactory.INSTANCE.makeCsvRecord();
        record2 = RecordFactory.INSTANCE.makeCsvRecord();
        barDelimitedRecord = RecordFactory.INSTANCE.makeDelimitedRecord(BAR);

        emptyReport = new DelimitedValueReport(emptyRecordsGateway, reportWriter);
    }


    @Test
    public void forNewDelimitedReport_writerReturnsEmptyString() {
        emptyReport.startWriting();
        assertEquals(EMPTY_STRING, reportWriter.result());
    }

    @Test
    public void forGatewayWithSingleEmptyRecord_writerReturnsEmptyString() {
        assertReportResults(EMPTY_STRING, record1);
    }

    @Test
    public void forGatewayOf2EmptyRecords_writerReturns2EmptyLinesString() {
        assertReportResults(EMPTY_STRING + lineSeparator(), record1, record2);
    }

    @Test
    public void forGatewayOfEmptyValue_writerReturnsEmptyString() {
        record1.addValue(new StringValue(EMPTY_STRING));
        assertReportResults(EMPTY_STRING, record1);
    }

    @Test
    public void forGatewayOfSomeValue_writerReturnsThatValue() {
        record1.addValue(new StringValue(VALUE_1));
        assertReportResults(VALUE_1, record1);
    }

    @Test
    public void forGatewayOfSomeUnwrappedStringValue_writerReturnsThatValue() {
        record1.addValue(VALUE_1);
        assertReportResults(VALUE_1, record1);
    }

    @Test
    public void forGatewayOf2EmptyValues_writerReturnsComma() {
        record1.addValue(new StringValue(EMPTY_STRING));
        record1.addValue(new StringValue(EMPTY_STRING));
        assertReportResults(COMMA, record1);
    }

    @Test
    public void forGatewayOf2Values_writerReturnsThe2ValuesSeparatedByComma() {
        record1.addValue(new StringValue(VALUE_1));
        record1.addValue(new StringValue(VALUE_2));
        assertReportResults(VALUE_1 + COMMA + VALUE_2, record1);
    }

    @Test
    public void whenAdd2Records_generateShouldReturnResultsIn2Lines() {
        record1.addValue(new StringValue(VALUE_1));
        record2.addValue(new StringValue(VALUE_2));
        assertReportResults(VALUE_1 + lineSeparator() + VALUE_2, record1, record2);
    }

    @Test
    public void whenAddIntegerValue_generateReturnsSameIntegerString() {
        record1.addValue(new StringValue(10 + ""));
        assertReportResults("10", record1);
    }

    @Test
    public void whenAddIntegerValueAndStringValue_generateReturnsSameIntegerAndString() {
        record1.addValue(new StringValue(10 + ""));
        record1.addValue(new StringValue(VALUE_1));
        assertReportResults("10" + COMMA + VALUE_1, record1);
    }

    @Test
    public void whenAddDoubleValue_generateReturnsSameDoubleValueString() {
        record1.addValue(new StringValue("10.0"));
        assertReportResults("10.0", record1);
    }

    @Test
    public void whenAddDoubleAndStringValues_generateReturnsSameDoubleAndString() {
        record1.addValue(new StringValue(10.0 + ""));
        record1.addValue(new StringValue(VALUE_1));
        assertReportResults("10.0" + COMMA + VALUE_1, record1);
    }

    @Test
    public void whenAddDateValue_generateReturnsSimpleFormattedDate() {
        record1.addValue(new DateValue(new Date()));
        assertReportResults(simpleFormattedTestDate(new Date()), record1);
    }

    @Test
    public void whenAddingBigDecimalValue_generateReturnsBigDecimalString() {
        record1.addValue(new BigDecimalValue(new BigDecimal("12345678.887")));
        assertReportResults("12345678.887", record1);
    }

    @Test
    public void canUseDifferentDelimiter() {
        barDelimitedRecord.addValue(new StringValue(VALUE_1));
        barDelimitedRecord.addValue(new StringValue(VALUE_2));
        assertReportResults(VALUE_1 + BAR + VALUE_2, barDelimitedRecord);
    }

    @Test
    public void canAddRecordsWithDifferentDelimiters() {
        record1.addValue(new StringValue(VALUE_1));
        record1.addValue(new StringValue(VALUE_2));

        barDelimitedRecord.addValue(new StringValue(VALUE_1));
        barDelimitedRecord.addValue(new StringValue(VALUE_2));

        assertReportResults(VALUE_1 + COMMA + VALUE_2 + lineSeparator() + VALUE_1 + BAR + VALUE_2, record1, barDelimitedRecord);
    }

    @Test
    public void canDefineReportWithRecordsGateWay() {
        new DelimitedValueReport(recordsFeeder, reportWriter);
    }

    @Test
    public void whenCreatingReportForEmptyRecordsGatewayWithReportWriter_writer_ReturnsEmptyString() {
        assertEquals(EMPTY_STRING, reportWriter.result());
    }

    @Test
    public void whenCreatingReportForEmptyRecordsGatewayWithReportWriter_afterGenerateWriter_ReturnsEmptyString() {
        emptyReport.startWriting();
        assertEquals(EMPTY_STRING, reportWriter.result());
    }

    @Test
    public void whenCreatingReportForSingleEmptyRecord_afterGenerateReportWriterReturnsEmptyString() {
        assertReportResults(EMPTY_STRING, record1);
    }

    @Test
    public void whenAdding2EmptyRecords_afterGenerateReportWriterReturnsEmptyLinesString() {
        assertReportResults(EMPTY_STRING + lineSeparator(), record1, record2);
    }

    @Test(expected = DefaultRecord.ValueContainsRecordDelimiterException.class)
    public void givenARecordWithValueContainsRecordDelimiter_valueShouldNotContainRecordDelimiter() {
        record1.addValue("Sample,Sample");
        record1.toLine();
    }

    @Test
    public void callingToLineForARecordWithDiscriminator_returnsALineThatBeginsWithThatDiscriminator(){
        Record record = RecordFactory.INSTANCE.makeDiscriminatedRecord(COMMA, "D");
        assertTrue(record.toLine().startsWith("D"));
    }
    private String lineSeparator() {
        return System.getProperty("line.separator");
    }

    private String simpleFormattedTestDate(Date date) {
        return new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(date);
    }

    private void assertReportResults(String expectedResult, Record... records) {
        new DelimitedValueReport(new CollectionRecordsFeeder(Arrays.asList(records)), reportWriter).startWriting();
        assertEquals(expectedResult, reportWriter.result());
    }


}
