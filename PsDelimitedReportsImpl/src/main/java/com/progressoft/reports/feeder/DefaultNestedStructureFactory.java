package com.progressoft.reports.feeder;

class DefaultNestedStructureFactory implements NestedStructureFactory {

    @Override
    public NestedStructure makeNestedStructure(final String fieldName, final ClassStructure classStructure) {
        return new DefaultNestedStructure(fieldName, classStructure);
    }

    @Override
    public NestedStructure makeNestedStructure(final String fieldName, final ClassStructure classStructure, final FetchStrategy fetchStrategy) {
        return new DefaultNestedStructure(fieldName, classStructure, fetchStrategy);
    }
}
