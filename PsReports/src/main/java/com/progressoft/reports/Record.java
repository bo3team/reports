package com.progressoft.reports;

public interface Record {

    String toLine();

    Record addValue(final FormattableValue v);

    Record addValue(final String v);

}
