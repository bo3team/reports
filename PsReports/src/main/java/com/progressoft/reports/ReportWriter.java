package com.progressoft.reports;

public interface ReportWriter {

    void writeLine(String line);

    void writeLastLine(String line);
}
