package com.progressoft.reports;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateValue implements FormattableValue {

    private static final String DEFAULT_SIMPLE_DATE_FORMAT = "dd/MM/yyyy";

    private final Date value;
    private final SimpleDateFormat formatter;

    public DateValue(Date value) {
        this.value = value;
        this.formatter = new SimpleDateFormat(DEFAULT_SIMPLE_DATE_FORMAT);
    }

    public DateValue(Date value, String dateFormatPattern) {
        this.value = value;
        this.formatter = new SimpleDateFormat(dateFormatPattern);
    }

    public String formatted() {
        return value==null?"":formatter.format(value);
    }
}
