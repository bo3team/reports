package com.progressoft.reports.feeder;

import com.progressoft.reports.Record;
import com.progressoft.reports.feeder.formatter.ReflectionFormatter;

import java.util.Collection;

interface ClassStructure {

    @FunctionalInterface
    interface ClassStructureListener {
        void onNewCollectionFound(final Collection<?> objects, final ClassStructure classStructure);
    }

    void addField(final ReflectionFormatter field);

    void addFields(final Collection<ReflectionFormatter> fields);

    void addFields(final ReflectionFormatter... fields);

    void addNestedStructure(final NestedStructure nestedStructure);

    void nestedClassStructures(final ClassStructureListener classStructureListener, final Object rootObject);

    Record createRecord(final Object rootObject);
}
