package com.progressoft.reports.feeder;

import com.progressoft.reports.RecordFactory;
import com.progressoft.reports.RecordsFeeder;
import com.progressoft.reports.StringValue;
import com.progressoft.reports.feeder.formatter.ReflectionFormatter;
import com.progressoft.reports.feeder.formatter.ValueFormatter;
import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(HierarchicalContextRunner.class)
public class ReflectionFeederTest {

    public static final String STRING_VALUE_FIELD_NAME = "stringValue";
    public static final String EMPTY_NESTED_RECORDS_FIELD_NAME = "emptyNestedRecords";
    public static final String NULL_MAP_RECORDS_FIELD_NAME = "nullMapRecords";
    public static final String EMPTY_MAP_RECORDS_FIELD_NAME = "emptyMapRecords";
    public static final String SINGLE_ENTRY_NESTED_MAP_FIELD_NAME = "singleEntryNestedMap";

    static Date getSampleDate() {
        Calendar c = Calendar.getInstance();
        c.set(2016, Calendar.JULY, 20);
        return c.getTime();
    }

    static final String EMPTY_STRING = "";
    static final String NOT_IN_CLASS_FIELD = "notInClassField";
    static final String NULL_FIELD_NAME = "nullField";
    static final String SIMPLE_STRING_FIELD_NAME = "simpleStringField";
    static final String OTHER_SIMPLE_STRING_FIELD_NAME = "anotherSimpleStringField";
    static final String DATE_FIELD_NAME = "dateField";
    static final String dd_MM_yyyy = "dd/MM/yyyy";
    static final String DATE_STRING_FORMATTED_WITH_PATTERN_dd_MM_yyyy = new SimpleDateFormat(dd_MM_yyyy).format(ReflectionFeederTest.getSampleDate());
    static final String MM_dd_yyyy = "MM/dd/yyyy";
    static final String DATE_STRING_FORMATTED_WITH_PATTERN_MM_dd_yyyy = new SimpleDateFormat(MM_dd_yyyy).format(ReflectionFeederTest.getSampleDate());
    static final String INTEGER_FIELD_NAME = "integerField";
    static final String BIG_DECIMAL_FIELD_NAME = "bigDecimalField";
    static final String FLOAT_FIELD_NAME = "floatField";
    static final String DOUBLE_FIELD_NAME = "doubleField";
    static final String NULL_INTEGER_FIELD_NAME = "nullIntegerField";
    static final String NESTED_NULL_OBJECT_FIELD_NAME = "nestedNullObjectField";
    static final String NESTED_OBJECT_FIELD_NAME = "nestedObjectField";
    static final String NULL_COLLECTION_FIELD_NAME = "nullCollectionField";
    static final String EMPTY_COLLECTION_FIELD_NAME = "emptyCollectionField";
    static final String COLLECTION_FIELD_NAME = "collectionField";
    static final String TWO_ITEMS_COLLECTION_FIELD_NAME = "twoItemsCollectionField";
    static final String INTEGER_COLLECTION_FIELD_NAME = "integerCollectionField";
    static final String TWO_INTEGER_COLLECTION_FIELD_NAME = "twoIntegerCollectionField";
    static final String ANOTHER_STRING_FIELD_NAME = "anotherString";
    public static final String NESTED_RECORDS_FIELD_NAME = "nestedRecords";

    static final String BAR_DELIMITER = "|";
    static final String COMMA_DELIMITER = ",";
    static final TestObject TEST_OBJECT = new TestObject();

    private static ClassStructure classStructure;
    private List<Object> objectsCollection;
    private static RecordsFeeder recordsFeeder;

    private static final ValueFormatter NESTED_TEST_OBJECT_FORMATTER = new ValueFormatter() {
        @Override
        public String formatFieldValue(Object value) {
            return value == null ? new StringValue(EMPTY_STRING).formatted() : new StringValue(((NestedTestObject) value).getStringValue()).formatted();
        }
    };

    private String feedRecords(int times) {
        String result = "";
        for (int i = 0; i < times - 1; i++)
            result += recordsFeeder.next().toLine() + "\n";

        result += recordsFeeder.next().toLine();
        return result;
    }

    public class EmptyCollectionContext {
        @Before
        public void setup_givenGateWayInitializedWithEmptyObjectsCollection() {
            objectsCollection = new ArrayList<>();
            classStructure = ClassStructureBuilder.make().build();
            recordsFeeder = new ReflectionFeeder(classStructure, objectsCollection);
        }

        @Test(expected = NoSuchElementException.class)
        public void _callingFeederNextThrowsNoSuchElementException() {
            recordsFeeder.next();
        }

        @Test
        public void _callingHasNextForEmptyFeeder_returnsFalse() {
            assertFalse(recordsFeeder.hasNext());
        }
    }

    public class SingleObjectCollectionContext {

        @Before
        public void givenFeederInitializedWithSingleRecordCollection() {
            objectsCollection = new ArrayList<Object>() {{
                add(TEST_OBJECT);
            }};
            classStructure = ClassStructureBuilder.make().build();
            recordsFeeder = new ReflectionFeeder(classStructure, objectsCollection);
        }

        @Test
        public void callingHasNextForNoneEmptyFeeder_returnsTrue() {
            assertTrue(recordsFeeder.hasNext());
        }

        @Test
        public void givenEmptyClassStructure_callingFeederNextReturnsARecordThatRepresentAnEmptyString() {
            assertEquals(EMPTY_STRING, recordsFeeder.next().toLine());
        }

        @Test(expected = ReflectionFormatter.FieldNameDoesNotExistInClass.class)
        public void givenClassStructureWithOneFieldThatDoesNotExistInCollectionObjectsClass_callingFeederNextThrowsException() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(NOT_IN_CLASS_FIELD));
            recordsFeeder.next();
        }

        @Test
        public void givenThatObjectHasAStringFieldWithNullValueAndAClassStructureForJustThatField_callingNextReturnsAnEmptyStringRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(NULL_FIELD_NAME));
            assertEquals(EMPTY_STRING, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasAStringFieldWithValueAndAClassStructureForJustThatField_callingNextReturnsThatFieldStringValueRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            assertEquals(TEST_OBJECT.getSimpleStringField(), recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasAStringFieldWithValueAndAClassStructureForJustThatField_callingNextReturnsThatFieldStringValueRecordCreatedByTheRecordFactory() {
            ClassStructure cs = ClassStructureBuilder.make().withRecordFactory(RecordFactory.INSTANCE).build();
            cs.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            RecordsFeeder recordsFeeder = new ReflectionFeeder(cs, objectsCollection);
            assertEquals(TEST_OBJECT.getSimpleStringField(), recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHas2StringFieldsInitializedToNoneNullValueAndClassStructureForThese2Fields_callingFeederNextGeneratesARecordToMatchThe2ValuesSeparatedByComma() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));

            assertEquals(TEST_OBJECT.getSimpleStringField() +
                            COMMA_DELIMITER +
                            TEST_OBJECT.getAnotherSimpleStringField(),
                    recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasANumericValueAndAClassStructureForThatField_callingFeederNextReturnsThatNumberStringMatchingRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(INTEGER_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(FLOAT_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(DOUBLE_FIELD_NAME));
            assertEquals(TEST_OBJECT.getIntegerField() +
                            COMMA_DELIMITER +
                            TEST_OBJECT.getFloatField() +
                            COMMA_DELIMITER +
                            TEST_OBJECT.getDoubleField(),
                    recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNumericFieldWithNullValueAndAClassStructureForThatFieldWithDefaultNullValue_callingFeederNextReturnsARecordThatMatchTheDefaultNullValueString() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(NULL_INTEGER_FIELD_NAME, 0));
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(NULL_INTEGER_FIELD_NAME, 1));
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(NULL_INTEGER_FIELD_NAME, 2));
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(NULL_INTEGER_FIELD_NAME, 3));
            classStructure.addField(ReflectionFormatter.FACTORY.makeNumberField(NULL_INTEGER_FIELD_NAME, "4"));
            assertEquals("0,1,2,3,4", recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasDateFieldAndAClassStructureForThatField_callingFeederNextReturnsARecordThatMatchTheFormattedDateStringForPattern_dd_MM_yyyy() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeDateField(DATE_FIELD_NAME));
            assertEquals(DATE_STRING_FORMATTED_WITH_PATTERN_dd_MM_yyyy, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasDateFieldAndAClassStructureForThatFieldWithPattern_MM_dd_yyyy_callingFeederNextReturnsARecordThatMatchTheFormattedDateStringForPattern_MM_dd_yyyy() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeDateField(DATE_FIELD_NAME, MM_dd_yyyy));
            assertEquals(DATE_STRING_FORMATTED_WITH_PATTERN_MM_dd_yyyy, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasBigDecimalFieldAndAClassStructureForThatField_callingFeederNextReturnsARecordThatMatchTheBigDecimalFieldPlainStringValue() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeBigDecimalField(BIG_DECIMAL_FIELD_NAME));
            assertEquals(TEST_OBJECT.getBigDecimalField().toPlainString(), recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedObjectFieldWithNullValueAndAClassStructureForThatField_callingFeederNextReturnsAnEmptyStringRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeObjectField(NESTED_NULL_OBJECT_FIELD_NAME));
            assertEquals(EMPTY_STRING, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedObjectFieldAndThatFieldValueIsNotNullAndAClassStructureForThatFieldWithoutSpecialFormatter_callingFeederNextReturnsTheNestedObjectToStringValueMatchingRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeObjectField(NESTED_OBJECT_FIELD_NAME));
            assertEquals(TEST_OBJECT.getNestedObjectField().toString(), recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedObjectFieldAndThatFieldValueIsNotNullAndAClassStructureForThatFieldWithASpecialFormatter_callingFeederNextReturnsTheNestedObjectStringValueFormattedByTheFormatter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeObjectField(NESTED_OBJECT_FIELD_NAME, NESTED_TEST_OBJECT_FORMATTER));
            assertEquals(TEST_OBJECT.getNestedObjectField().getStringValue(), recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatIsNullAndAClassStructureOfObjectFieldForThatField_callingFeederNextReturnsAnEmptyStringMatchingRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeObjectField(NULL_COLLECTION_FIELD_NAME));
            assertEquals(EMPTY_STRING, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatIsNullAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnsAnEmptyStringMatchingRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(NULL_COLLECTION_FIELD_NAME, BAR_DELIMITER, ValueFormatter.STRING_FORMATTER));
            assertEquals(EMPTY_STRING, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatFieldValueIsEmptyCollectionAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnsAnEmptyStringMatchingRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(EMPTY_COLLECTION_FIELD_NAME, BAR_DELIMITER, ValueFormatter.STRING_FORMATTER));
            assertEquals(EMPTY_STRING, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatFieldValueIsASingleItemCollectionAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnTheSingleItemStringMatchingRecordFormattedByTheFormatter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(COLLECTION_FIELD_NAME, BAR_DELIMITER, ValueFormatter.STRING_FORMATTER));
            assertEquals(TestObject.SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatFieldValueIsASingleItemCollectionAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnTheSingleItemStringMatchingRecordCreatedBySpecificRecordFactoryFormattedByTheFormatter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(COLLECTION_FIELD_NAME, BAR_DELIMITER, ValueFormatter.STRING_FORMATTER, RecordFactory.INSTANCE));
            assertEquals(TestObject.SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatFieldValueIsA2ItemsCollectionAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnsThe2ItemsStringMatchingRecordSeparatedByBarDelimiterAndFormattedByTheFormatter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(TWO_ITEMS_COLLECTION_FIELD_NAME, BAR_DELIMITER, ValueFormatter.STRING_FORMATTER));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + BAR_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test(expected = Exception.class)
        public void givenThatObjectHasACollectionFieldAndThatFieldValueIsA2ItemsCollectionAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnsThe2ItemsStringMatchingRecordSeparatedByCommaDelimiterAndFormattedByTheFormatter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(TWO_ITEMS_COLLECTION_FIELD_NAME, COMMA_DELIMITER, ValueFormatter.STRING_FORMATTER));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatFieldValueIsASingleIntegerItemCollectionAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnTheSingleIntegerItemStringMatchingRecordFormattedByTheFormatter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(INTEGER_COLLECTION_FIELD_NAME, BAR_DELIMITER, ValueFormatter.INTEGER_FORMATTER));
            assertEquals(TestObject.INTEGER_10 + "", recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasACollectionFieldAndThatFieldValueIsA2IntegerItemCollectionAndAClassStructureOfCollectionFieldForThatField_callingFeederNextReturnThe2IntegerItemStringSeparatedByBarMatchingRecordFormattedByTheFormatter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeCollectionField(TWO_INTEGER_COLLECTION_FIELD_NAME, BAR_DELIMITER, ValueFormatter.INTEGER_FORMATTER));
            assertEquals(TestObject.INTEGER_10 + BAR_DELIMITER + TestObject.INTEGER_20, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedRecordsCollectionFieldThatIsNullAndANestedClassStructureForThatField_callingFeederNextReturnsTheRootObjectMatchingRecordOnly() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));
            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(NULL_COLLECTION_FIELD_NAME, ClassStructureBuilder.make().build()));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedRecordsMapCollectionFieldThatIsNullAndANestedClassStructureForThatField_callingFeederNextReturnsTheRootObjectMatchingRecordOnly() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));
            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(NULL_MAP_RECORDS_FIELD_NAME, ClassStructureBuilder.make().build()));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedRecordsMapCollectionFieldThatIsEmptyAndANestedClassStructureForThatField_callingFeederNextReturnsTheRootObjectMatchingRecordOnly() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));
            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(EMPTY_MAP_RECORDS_FIELD_NAME, ClassStructureBuilder.make().build(), FetchStrategy.mapFetchStrategy));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedRecordsMapCollectionFieldThatHasOneEntryAndANestedClassStructureForThatField_callingFeederNextTwiceReturnsTheRootObjectMatchingRecordAndTheMapEntryRecord() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));

            ClassStructure nestedClassStructure = ClassStructureBuilder.make().build();
            nestedClassStructure.addField(ReflectionFormatter.FACTORY.makeStringField(NestedTestObject.STRING_VALUE));
            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(SINGLE_ENTRY_NESTED_MAP_FIELD_NAME, nestedClassStructure, FetchStrategy.mapFetchStrategy));
            assertEquals(TEST_OBJECT.getSimpleStringField() + COMMA_DELIMITER + TEST_OBJECT.getAnotherSimpleStringField() + "\n" + TEST_OBJECT.getNestedObjectField().getStringValue(), feedRecords(2));
        }

        @Test
        public void givenThatObjectHasNestedRecordsCollectionFieldThatIsEmptyAndANestedClassStructureForThatField_callingFeederNextReturnsTheRootObjectMatchingRecordOnly() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));
            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(EMPTY_COLLECTION_FIELD_NAME, ClassStructureBuilder.make().build()));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE, recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedRecordsCollectionFieldThatHasSingleItemAndANestedClassStructureForThatFieldFields_callingFeederNextTwiceReturnsTheRootObjectMatchingRecordAndTheNestedCollectionItemRecord() {
            classStructure.addFields(new ArrayList<ReflectionFormatter>() {
                {
                    add(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
                    add(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));
                }
            });
            ClassStructure nestedClassStructure = ClassStructureBuilder.make().build();
            nestedClassStructure.addField(ReflectionFormatter.FACTORY.makeStringField(STRING_VALUE_FIELD_NAME));

            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(NESTED_RECORDS_FIELD_NAME, nestedClassStructure));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE +
                            "\n" + NestedTestObject.STRING_VALUE
                    , recordsFeeder.next().toLine() + "\n" + recordsFeeder.next().toLine());
        }

        @Test
        public void givenThatObjectHasNestedRecordsCollectionFieldThatHasSingleItemAndANestedClassStructureForThatFieldFieldsWithBarDelimiter_callingFeederNextTwiceReturnsTheRootObjectMatchingRecordAndTheNestedCollectionItemRecordDelimitedByBars() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));

            ClassStructure nestedClassStructure = ClassStructureBuilder.make().withDelimiter(BAR_DELIMITER).build();
            nestedClassStructure.addFields(ReflectionFormatter.FACTORY.makeStringField(STRING_VALUE_FIELD_NAME), ReflectionFormatter.FACTORY.makeStringField(ANOTHER_STRING_FIELD_NAME));

            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(NESTED_RECORDS_FIELD_NAME, nestedClassStructure));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE +
                            "\n" + NestedTestObject.STRING_VALUE + BAR_DELIMITER + NestedTestObject.ANOTHER_STRING
                    , recordsFeeder.next().toLine() + "\n" + recordsFeeder.next().toLine());
        }

        @Test(expected = NoSuchElementException.class)
        public void givenThatObjectHasEmptyNestedRecordsCollectionFieldThatHasSingleItemAndANestedClassStructureForThatFieldFields_callingFeederNextTwiceThrowsNoSuchElementException() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));

            ClassStructure nestedClassStructure = ClassStructureBuilder.make().build();
            nestedClassStructure.addField(ReflectionFormatter.FACTORY.makeStringField(STRING_VALUE_FIELD_NAME));

            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(EMPTY_NESTED_RECORDS_FIELD_NAME, nestedClassStructure));
            recordsFeeder.next();
            recordsFeeder.next();
        }

        @Test(expected = ReflectionFormatter.FieldNameDoesNotExistInClass.class)
        public void givenThatObjectHasNestedRecordsCollectionFieldThatHasSingleItemAndANestedClassStructureForAFieldThatDoesNotExistInTheNestedObjects_callingFeederNextTwiceThrowsFieldNameDoesNotExistInClassException() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));

            ClassStructure nestedClassStructure = ClassStructureBuilder.make().build();
            nestedClassStructure.addField(ReflectionFormatter.FACTORY.makeStringField(STRING_VALUE_FIELD_NAME));

            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(NOT_IN_CLASS_FIELD, nestedClassStructure));
            recordsFeeder.next();
        }
    }

    public class TowObjectCollectionContext {

        @Before
        public void givenFeederInitializedWithSingleRecordCollection() {
            objectsCollection = new ArrayList<Object>() {{
                add(TEST_OBJECT);
                add(TEST_OBJECT);
            }};
            classStructure = ClassStructureBuilder.make().build();
            recordsFeeder = new ReflectionFeeder(classStructure, objectsCollection);
        }

        @Test
        public void givenThatThe2ObjectsHasNestedRecordsCollectionFieldThatHasSingleItemAndANestedClassStructureForThatFieldFieldsWithBarDelimiter_callingFeederNextTwiceReturnsTheRootObjectMatchingRecordAndTheNestedCollectionItemRecordDelimitedByBars() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(OTHER_SIMPLE_STRING_FIELD_NAME));

            ClassStructure nestedClassStructure = ClassStructureBuilder.make().withDelimiter(BAR_DELIMITER).build();
            nestedClassStructure.addFields(ReflectionFormatter.FACTORY.makeStringField(STRING_VALUE_FIELD_NAME), ReflectionFormatter.FACTORY.makeStringField(ANOTHER_STRING_FIELD_NAME));

            classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(NESTED_RECORDS_FIELD_NAME, nestedClassStructure));
            assertEquals(TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE +
                            "\n" + NestedTestObject.STRING_VALUE + BAR_DELIMITER + NestedTestObject.ANOTHER_STRING +
                            "\n" + TestObject.SIMPLE_STRING_VALUE + COMMA_DELIMITER + TestObject.OTHER_SIMPLE_STRING_VALUE +
                            "\n" + NestedTestObject.STRING_VALUE + BAR_DELIMITER + NestedTestObject.ANOTHER_STRING
                    , feedRecords(4));
        }

    }

    public class ClassStructureWithDiscriminator {

        public static final String DISCREMENATOR = "D";

        @Before
        public void givenFeederInitializedWithSingleRecordCollection() {
            objectsCollection = new ArrayList<Object>() {{
                add(TEST_OBJECT);
            }};

            classStructure = ClassStructureBuilder.make().withDiscriminator(DISCREMENATOR).build();
            recordsFeeder = new ReflectionFeeder(classStructure, objectsCollection);
        }

        @Test
        public void givenAClassStructureWithDiscriminator_recordsShouldStartWIthThatDiscrimnator() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            assertEquals(DISCREMENATOR + COMMA_DELIMITER + TEST_OBJECT.getSimpleStringField(), recordsFeeder.next().toLine());
        }
    }

    public class ClassStructureWithDiscriminatorAndDelimiter {

        public static final String DISCREMENATOR = "B";

        @Before
        public void givenFeederInitializedWithSingleRecordCollection() {
            objectsCollection = new ArrayList<Object>() {{
                add(TEST_OBJECT);
            }};

            classStructure = ClassStructureBuilder.make().withDelimiter(BAR_DELIMITER).withDiscriminator(DISCREMENATOR).build();
            recordsFeeder = new ReflectionFeeder(classStructure, objectsCollection);
        }

        @Test
        public void givenAClassStructureWithDiscriminatorAndDelimiter_recordsShouldStartWIthThatDiscrimnatorAndSeparatedByTheDelimiter() {
            classStructure.addField(ReflectionFormatter.FACTORY.makeStringField(SIMPLE_STRING_FIELD_NAME));
            assertEquals(DISCREMENATOR + BAR_DELIMITER + TEST_OBJECT.getSimpleStringField(), recordsFeeder.next().toLine());
        }
    }
}