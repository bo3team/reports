package com.progressoft.reports.feeder;

import com.progressoft.reports.Record;
import com.progressoft.reports.RecordsFeeder;

import java.util.Collection;
import java.util.Iterator;

public class CollectionRecordsFeeder implements RecordsFeeder {

    private final Iterator<Record> iterator;

    public CollectionRecordsFeeder(final Collection<Record> r) {
        this.iterator = r.iterator();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public Record next() {
        return iterator.next();
    }
}