package com.progressoft.reports;

public class StringValue implements FormattableValue {

    private final String value;

    public StringValue(String value){
        this.value=value;
    }

    public String formatted() {
        return value==null?"":value;
    }
}
