package com.progressoft.reports.feeder;

@FunctionalInterface
public interface ValueExtractor {

    Object extractValue(final Object rootObject, final String fieldName);

}
