package com.progressoft.reports.feeder;

import java.math.BigDecimal;
import java.util.*;

public class TestObject {

    public static final String SIMPLE_STRING_VALUE = "simpleStringValue";
    public static final String OTHER_SIMPLE_STRING_VALUE = "otherSimpleStringValue";
    public static final int INTEGER_10 = 10;
    public static final int INTEGER_20 = 20;

    private final String nullField = null;
    private final String simpleStringField = SIMPLE_STRING_VALUE;
    private final String anotherSimpleStringField = OTHER_SIMPLE_STRING_VALUE;
    private final Date dateField = ReflectionFeederTest.getSampleDate();
    private final int integerField = INTEGER_10;
    private final BigDecimal bigDecimalField = new BigDecimal("123456789.0990");
    private final float floatField = 20.0f;
    private final double doubleField = 10.0;
    private final Integer nullIntegerField = null;
    private final NestedTestObject nestedNullObjectField = null;
    private final NestedTestObject nestedObjectField = new NestedTestObject();
    private final Collection<Object> nullCollectionField = null;
    private final Collection<Object> emptyCollectionField = new ArrayList<>();
    private final Collection<String> collectionField = new ArrayList<String>() {{
        add(SIMPLE_STRING_VALUE);
    }};
    private final Collection<Integer> integerCollectionField = new ArrayList<Integer>() {{
        add(INTEGER_10);
    }};

    private final Collection<String> twoItemsCollectionField = new ArrayList<String>() {{
        add(SIMPLE_STRING_VALUE);
        add(OTHER_SIMPLE_STRING_VALUE);
    }};

    private final Collection<Integer> twoIntegerCollectionField = new ArrayList<Integer>() {{
        add(INTEGER_10);
        add(INTEGER_20);
    }};

    private final Collection<NestedTestObject> nestedRecords = new ArrayList<NestedTestObject>() {{
        add(new NestedTestObject());
    }};
    private final Collection<NestedTestObject> emptyNestedRecords = new ArrayList<>();
    private final Map<String, NestedTestObject> nullMapRecords = null;
    private final Map<String, NestedTestObject> emptyMapRecords = new HashMap<>();
    private final Map<String, NestedTestObject> singleEntryNestedMap = new HashMap<String, NestedTestObject>(){{
        put("record1", new NestedTestObject());
    }};


    public TestObject() {
    }

    public String getNullField() {
        return nullField;
    }

    public String getSimpleStringField() {
        return simpleStringField;
    }

    public String getAnotherSimpleStringField() {
        return anotherSimpleStringField;
    }

    public Date getDateField() {
        return dateField;
    }

    public int getIntegerField() {
        return integerField;
    }

    public BigDecimal getBigDecimalField() {
        return bigDecimalField;
    }

    public float getFloatField() {
        return floatField;
    }

    public double getDoubleField() {
        return doubleField;
    }

    public Integer getNullIntegerField() {
        return nullIntegerField;
    }

    public NestedTestObject getNestedNullObjectField() {
        return nestedNullObjectField;
    }

    public NestedTestObject getNestedObjectField() {
        return nestedObjectField;
    }

    public Collection<Object> getNullCollectionField() {
        return nullCollectionField;
    }

    public Collection<Object> getEmptyCollectionField() {
        return emptyCollectionField;
    }

    public Collection<String> getCollectionField() {
        return collectionField;
    }

    public Collection<String> getTwoItemsCollectionField() {
        return twoItemsCollectionField;
    }

    public Collection<Integer> getIntegerCollectionField() {
        return integerCollectionField;
    }

    public Collection<Integer> getTwoIntegerCollectionField() {
        return twoIntegerCollectionField;
    }

    public Collection<NestedTestObject> getNestedRecords() {
        return nestedRecords;
    }

    public Collection<NestedTestObject> getEmptyNestedRecords() {
        return emptyNestedRecords;
    }

    public Map<String, NestedTestObject> getEmptyMapRecords() {
        return emptyMapRecords;
    }

    public Map<String, NestedTestObject> getSingleEntryNestedMap() {
        return singleEntryNestedMap;
    }

    public Map<String, NestedTestObject> getNullMapRecords() {
        return nullMapRecords;
    }
}
