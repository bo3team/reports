package com.progressoft.reports.feeder;

import com.progressoft.reports.RecordFactory;

class DefaultClassStructureBuilder implements ClassStructureBuilder {

    private String delimiter = ",";
    private String discriminator = null;
    private RecordFactory recordFactory = RecordFactory.INSTANCE;

    @Override
    public ClassStructure build() {
        return new ReflectionClassStructure(delimiter, discriminator, recordFactory);
    }

    @Override
    public ClassStructureBuilder withDelimiter(String delimiter) {
        this.delimiter = delimiter;
        return this;
    }

    @Override
    public ClassStructureBuilder withDiscriminator(String discriminator) {
        this.discriminator = discriminator;
        return this;
    }

    @Override
    public ClassStructureBuilder withRecordFactory(RecordFactory recordFactory) {
        this.recordFactory = recordFactory;
        return this;
    }
}
