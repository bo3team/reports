package com.progressoft.reports;

public interface RecordsFeeder {

    boolean hasNext();

    Record next();
}
