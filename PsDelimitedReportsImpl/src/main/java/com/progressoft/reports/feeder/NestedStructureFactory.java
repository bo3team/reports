package com.progressoft.reports.feeder;

public interface NestedStructureFactory {
    NestedStructureFactory INSTANCE = new DefaultNestedStructureFactory();

    NestedStructure makeNestedStructure(final String fieldName, final ClassStructure classStructure);

    NestedStructure makeNestedStructure(final String fieldName, final ClassStructure classStructure, final FetchStrategy fetchStrategy);
}
