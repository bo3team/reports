package com.progressoft.reports;

public interface FormattableValue {
    String formatted();
}
