package com.progressoft.reports;

import java.math.BigDecimal;

public class BigDecimalValue implements FormattableValue {

    private final BigDecimal value;

    public BigDecimalValue(BigDecimal value) {
        this.value=value;
    }

    public String formatted() {
        return value==null?"":value.toPlainString();
    }
}
