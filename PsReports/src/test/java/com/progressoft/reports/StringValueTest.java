package com.progressoft.reports;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringValueTest {

    @Test
    public void whenPassNull_formattedShouldNotReturnNull() throws Exception {
        assertNotNull(new StringValue(null).formatted());
    }

    @Test
    public void whenPassNull_formattedReturnsEmptyString() throws Exception {
        assertEquals("", new StringValue(null).formatted());
    }

    @Test
    public void whenPassAnyString_formattedShouldReturnThatString() throws Exception {
        assertEquals("Test String", new StringValue("Test String").formatted());
    }
}
