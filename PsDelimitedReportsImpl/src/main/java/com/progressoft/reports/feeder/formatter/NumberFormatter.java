package com.progressoft.reports.feeder.formatter;

import com.progressoft.reports.StringValue;

import java.util.Objects;

public class NumberFormatter implements ValueFormatter{

    private final String defaultValue;

    public NumberFormatter() {
        this.defaultValue = EMPTY_STRING;
    }

    public NumberFormatter(Number defaultValue) {
        this.defaultValue = defaultValue.toString();
    }

    public NumberFormatter(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String formatFieldValue(final Object value) {
        return Objects.isNull(value) ? new StringValue(this.defaultValue).formatted() : new StringValue(value + EMPTY_STRING).formatted();
    }
}
