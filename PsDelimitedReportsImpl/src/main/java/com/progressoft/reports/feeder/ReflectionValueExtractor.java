package com.progressoft.reports.feeder;

import com.progressoft.reports.feeder.formatter.ReflectionFormatter;

import java.lang.reflect.Field;

public class ReflectionValueExtractor implements ValueExtractor {

    @Override
    public Object extractValue(Object rootObject, String fieldName) {
        try {
            Field f = rootObject.getClass().getDeclaredField(fieldName);
            f.setAccessible(true);
            return f.get(rootObject);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new ReflectionFormatter.FieldNameDoesNotExistInClass("field [" + fieldName + "] does not exist in class.", e);
        }
    }
}
