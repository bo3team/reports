package com.progressoft.reports;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class DefaultRecord implements Record{

    private static final String EMPTY_INITIAL_CONTENT = "";
    private static final String COMMA = ",";
    private final String delimiter;

    private final List<FormattableValue> values = new ArrayList<>();
    private Iterator<FormattableValue> valuesIterator;

    DefaultRecord() {
        this.delimiter = COMMA;
    }

    DefaultRecord(String delimiter) {
        this.delimiter = delimiter;
    }

    DefaultRecord(String delimiter, Optional<String> discriminator) {
        this.delimiter = delimiter;
        if(discriminator.isPresent())
            addValue(discriminator.get());
    }

    @Override
    public String toLine() {
        valuesIterator = values.iterator();
        return toLine(EMPTY_INITIAL_CONTENT);
    }

    private String toLine(String initialString){
        if(valuesIterator.hasNext())
            return toLine(initialString+getFormatted(valuesIterator.next()) +
                    separator());
        return initialString;
    }

    private String getFormatted(final FormattableValue v) {
        if (v.formatted().contains(delimiter))
            throw new ValueContainsRecordDelimiterException();
        return v.formatted();
    }

    @Override
    public Record addValue(final String v) {
        addValue(new StringValue(v));
        return this;
    }

    @Override
    public Record addValue(final FormattableValue v) {
        values.add(v);
        return this;
    }

    private String separator() {
        return valuesIterator.hasNext() ? this.delimiter : "";
    }

    class ValueContainsRecordDelimiterException extends RuntimeException {
    }
}