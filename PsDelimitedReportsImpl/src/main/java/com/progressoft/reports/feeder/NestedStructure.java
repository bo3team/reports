package com.progressoft.reports.feeder;

@FunctionalInterface
public interface NestedStructure {
    void findNestedCollections(final ClassStructure.ClassStructureListener classStructureListener, final Object rootObject);
}
