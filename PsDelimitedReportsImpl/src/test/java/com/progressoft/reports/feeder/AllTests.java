package com.progressoft.reports.feeder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CollectionRecordsFeederTest.class, ReflectionFeederTest.class
})
public class AllTests {
}
