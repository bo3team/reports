package com.progressoft.reports.feeder;

import com.progressoft.reports.ReportWriter;

class StringReportWriter implements ReportWriter {
    private String lines = "";

    String result() {
        return lines;
    }

    public void writeLine(String line) {
        lines += line + lineSeparator();
    }

    public void writeLastLine(String line) {
        lines += line;
    }

    private String lineSeparator() {
        return System.getProperty("line.separator");
    }
}
