package com.progressoft.reports.feeder;

public class NestedTestObject {


    public static final String STRING_VALUE = "stringValue";
    public static final String ANOTHER_STRING = "anotherString";

    private final String stringValue = STRING_VALUE;
    private final String anotherString = ANOTHER_STRING;

    public String getStringValue() {
        return stringValue;
    }

    public String getAnotherString() {
        return anotherString;
    }
}
