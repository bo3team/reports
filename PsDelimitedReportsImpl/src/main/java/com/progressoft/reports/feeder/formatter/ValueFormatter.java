package com.progressoft.reports.feeder.formatter;

import com.progressoft.reports.BigDecimalValue;
import com.progressoft.reports.StringValue;

import java.math.BigDecimal;
import java.util.Objects;

public interface ValueFormatter {
    String EMPTY_STRING = "";

    ValueFormatter STRING_FORMATTER = value -> Objects.isNull(value) ? EMPTY_STRING : new StringValue(value + EMPTY_STRING).formatted();

    ValueFormatter BIG_DECIMAL_FORMATTER = value -> Objects.isNull(value) ? new StringValue(EMPTY_STRING).formatted() : new BigDecimalValue((BigDecimal) value).formatted();

    ValueFormatter INTEGER_FORMATTER = STRING_FORMATTER;
    ValueFormatter OBJECT_FORMATTER = STRING_FORMATTER;
    ValueFormatter NUMBER_FORMATTER = new NumberFormatter();
    ValueFormatter DATE_FORMATTER = new DateFormatter();

    String formatFieldValue(final Object value);



}
