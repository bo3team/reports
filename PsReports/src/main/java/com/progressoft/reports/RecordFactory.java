package com.progressoft.reports;

public interface RecordFactory {

    RecordFactory INSTANCE = new DefaultRecordFactory();

    Record makeCsvRecord();

    Record makeDelimitedRecord(String delimiter);

    Record makeDiscriminatedRecord(String delimiter, String discriminator);
}
