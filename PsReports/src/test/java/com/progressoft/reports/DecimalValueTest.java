package com.progressoft.reports;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class DecimalValueTest {

    @Test
    public void passingNull_thenFormattedShouldNotReturnNull() throws Exception {
        assertNotNull(new BigDecimalValue(null).formatted());
    }

    @Test
    public void passingNull_thenFormattedShouldReturnEmptyString() throws Exception {
        assertEquals("", new BigDecimalValue(null).formatted());
    }

    @Test
    public void passingAnyNumericValue_thenFormattedShouldReturnThatNumericValueString() throws Exception {
        assertEquals("10", new BigDecimalValue(new BigDecimal("10")).formatted());
        assertEquals("10.0", new BigDecimalValue(new BigDecimal("10.0")).formatted());
        assertEquals("-10.0", new BigDecimalValue(new BigDecimal("-10.0")).formatted());
        assertEquals("0.0", new BigDecimalValue(new BigDecimal("0.0")).formatted());
        assertEquals("0.0", new BigDecimalValue(new BigDecimal(".0")).formatted());
        assertEquals("0", new BigDecimalValue(new BigDecimal("0")).formatted());
    }

    @Test(expected = NumberFormatException.class)
    public void passingNoneNumericBigDecimal_shouldThrowNumberFormatException() throws Exception {
        new BigDecimalValue(new BigDecimal("abc")).formatted();
    }
}
