package com.progressoft.reports;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class DateValueTest {

    private static final String DEFAULT_SIMPLE_DATE_FORMAT_MM_dd_yyyy = "MM/dd/yyyy";
    private static final String UNSUPPORTED_DATE_FORMAT = "abc";

    private Date DATE_20_07_2016;

    @Before
    public void setUp() throws Exception {
        Calendar c=Calendar.getInstance();
        c.set(2016,Calendar.JULY,20);
        DATE_20_07_2016=c.getTime();
    }

    @Test
    public void whenPassNull_formattedShouldNotReturnNull() throws Exception {
        assertNotNull(new DateValue(null).formatted());
    }

    @Test
    public void passingSimpleDate_formattedShouldReturnSimpleFormattedDateString() throws Exception {
        assertEquals("20/07/2016", new DateValue(DATE_20_07_2016).formatted());
        assertEquals("07/20/2016", new DateValue(DATE_20_07_2016, DEFAULT_SIMPLE_DATE_FORMAT_MM_dd_yyyy).formatted());
    }

    @Test(expected = IllegalArgumentException.class)
    public void passingUnsupportedDateFormat_throwsUnsupportedDatePattern() throws Exception {
        new DateValue(DATE_20_07_2016, UNSUPPORTED_DATE_FORMAT).formatted();
    }
}
