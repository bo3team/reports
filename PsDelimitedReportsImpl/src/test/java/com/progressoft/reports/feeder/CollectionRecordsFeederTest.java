package com.progressoft.reports.feeder;

import com.progressoft.reports.Record;
import com.progressoft.reports.RecordsFeeder;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class CollectionRecordsFeederTest {

    private RecordsFeeder gateWay;

    @Before
    public void setUp() throws Exception {
        gateWay = new CollectionRecordsFeeder(new ArrayList<Record>());
    }

    @Test
    public void emptyCollectionGateway_returnsEmptyIterator() throws Exception {
        assertNotNull(gateWay);
        assertFalse(gateWay.hasNext());
    }

    @Test(expected = NoSuchElementException.class)
    public void CallingNextOnEmptyCollectionGateway_returnsNoSuchElementException() throws Exception {
      gateWay.next();
    }

}
