package com.progressoft.reports.feeder.formatter;

import com.progressoft.reports.DateValue;
import com.progressoft.reports.StringValue;

import java.util.Date;
import java.util.Objects;

public class DateFormatter implements ValueFormatter {

    private String pattern;

    public DateFormatter() {
    }

    public DateFormatter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public String formatFieldValue(final Object v) {
        return Objects.isNull(v) ? new StringValue(EMPTY_STRING).formatted() : formatDateValue((Date) v);
    }

    private String formatDateValue(Date d) {
        return isEmptyPattern() ? new DateValue(d).formatted() : new DateValue(d, this.pattern).formatted();
    }

    private boolean isEmptyPattern() {
        return this.pattern == null || this.pattern.isEmpty();
    }
}
