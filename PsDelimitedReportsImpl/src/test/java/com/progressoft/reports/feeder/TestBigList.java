package com.progressoft.reports.feeder;

import com.progressoft.reports.DelimitedValueReport;
import com.progressoft.reports.RecordsFeeder;
import com.progressoft.reports.ReportWriter;
import com.progressoft.reports.feeder.formatter.ReflectionFormatter;
import com.progressoft.reports.feeder.formatter.ValueFormatter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestBigList {

    @Before
    public void setUp() {

    }

    @Ignore
    @Test
    public void TestBigList() throws FileNotFoundException {
        List<Object> testObjects = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            testObjects.add(new TestObject());
        }

        ClassStructure classStructure = ClassStructureBuilder.make().build();

        List<ReflectionFormatter> fields = new ArrayList<ReflectionFormatter>() {{
            add(ReflectionFormatter.FACTORY.makeStringField(ReflectionFeederTest.NULL_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeStringField(ReflectionFeederTest.SIMPLE_STRING_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeStringField(ReflectionFeederTest.OTHER_SIMPLE_STRING_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeDateField(ReflectionFeederTest.DATE_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeNumberField(ReflectionFeederTest.INTEGER_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeBigDecimalField(ReflectionFeederTest.BIG_DECIMAL_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeNumberField(ReflectionFeederTest.FLOAT_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeNumberField(ReflectionFeederTest.DOUBLE_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeNumberField(ReflectionFeederTest.NULL_INTEGER_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeObjectField(ReflectionFeederTest.NESTED_NULL_OBJECT_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeObjectField(ReflectionFeederTest.NESTED_OBJECT_FIELD_NAME));
            add(ReflectionFormatter.FACTORY.makeCollectionField(ReflectionFeederTest.NULL_COLLECTION_FIELD_NAME, "|", ValueFormatter.OBJECT_FORMATTER));
            add(ReflectionFormatter.FACTORY.makeCollectionField(ReflectionFeederTest.EMPTY_COLLECTION_FIELD_NAME, "|", ValueFormatter.OBJECT_FORMATTER));
            add(ReflectionFormatter.FACTORY.makeCollectionField(ReflectionFeederTest.COLLECTION_FIELD_NAME, "|", ValueFormatter.STRING_FORMATTER));
            add(ReflectionFormatter.FACTORY.makeCollectionField(ReflectionFeederTest.INTEGER_COLLECTION_FIELD_NAME, "|", ValueFormatter.INTEGER_FORMATTER));
            add(ReflectionFormatter.FACTORY.makeCollectionField(ReflectionFeederTest.TWO_INTEGER_COLLECTION_FIELD_NAME, "|", ValueFormatter.INTEGER_FORMATTER));
        }};

        classStructure.addFields(fields);
        ClassStructure nestedClassStructure = ClassStructureBuilder.make().withDelimiter("|").build();
        nestedClassStructure.addFields(ReflectionFormatter.FACTORY.makeStringField(ReflectionFeederTest.STRING_VALUE_FIELD_NAME), ReflectionFormatter.FACTORY.makeStringField(ReflectionFeederTest.ANOTHER_STRING_FIELD_NAME));
        classStructure.addNestedStructure(NestedStructureFactory.INSTANCE.makeNestedStructure(ReflectionFeederTest.NESTED_RECORDS_FIELD_NAME, nestedClassStructure));

        RecordsFeeder recordsFeeder = new ReflectionFeeder(classStructure, testObjects);
        final PrintStream ps = System.out;

        ReportWriter reportWriter = new ReportWriter() {
            final PrintWriter pw = new PrintWriter(new FileOutputStream(new File("/home/bo3/Documents/test.txt")));

            @Override
            public void writeLine(String s) {
                pw.println(s);
            }

            @Override
            public void writeLastLine(String s) {
                pw.print(s);
                pw.flush();
            }
        };
        DelimitedValueReport dvr = new DelimitedValueReport(recordsFeeder, reportWriter);
        double stime = new Date().getTime();
        dvr.startWriting();
        double etime = new Date().getTime();
        System.out.println("total print time : " + ((etime - stime) / 1000));

//        for(int i=0;i<=100;i++)
//            System.out.println(i+" "+(i%3 == 0?"Fizz":"")+(i%5 == 0?"Buzz":""));

    }
}