package com.progressoft.reports.feeder.formatter;

import com.progressoft.reports.RecordFactory;
import com.progressoft.reports.feeder.ReflectionField;
import com.progressoft.reports.feeder.ReflectionFieldFactory;

public class DefaultReflectionFieldFactory implements ReflectionFieldFactory {

    @Override
    public ReflectionField makeBigDecimalField(final String fieldName) {
        return new ReflectionField(fieldName, ValueFormatter.BIG_DECIMAL_FORMATTER);
    }

    @Override
    public ReflectionField makeStringField(final String fieldName) {
        return new ReflectionField(fieldName, ValueFormatter.STRING_FORMATTER);
    }

    @Override
    public ReflectionField makeNumberField(final String fieldName) {
        return new ReflectionField(fieldName, ValueFormatter.NUMBER_FORMATTER);
    }

    @Override
    public ReflectionField makeNumberField(final String fieldName, final Number defaultValue) {
        return new ReflectionField(fieldName, new NumberFormatter(defaultValue));
    }

    @Override
    public ReflectionField makeNumberField(final String fieldName, final String defaultValue) {
        return new ReflectionField(fieldName, new NumberFormatter(defaultValue));
    }

    @Override
    public ReflectionField makeDateField(final String fieldName) {
        return new ReflectionField(fieldName, ValueFormatter.DATE_FORMATTER);
    }

    @Override
    public ReflectionField makeDateField(final String fieldName, final String pattern) {
        return new ReflectionField(fieldName, new DateFormatter(pattern));
    }

    @Override
    public ReflectionField makeObjectField(final String fieldName) {
        return new ReflectionField(fieldName, ValueFormatter.OBJECT_FORMATTER);
    }

    @Override
    public ReflectionField makeObjectField(final String fieldName, final ValueFormatter formatter) {
        return new ReflectionField(fieldName, formatter);
    }

    @Override
    public ReflectionField makeCollectionField(final String fieldName, final String delimiter, final ValueFormatter formatter) {
        return new ReflectionField(fieldName, new CollectionFormatter(delimiter, formatter));
    }

    @Override
    public ReflectionField makeCollectionField(final String fieldName, final String delimiter, final ValueFormatter formatter, final RecordFactory recordFactory) {
        return new ReflectionField(fieldName, new CollectionFormatter(delimiter, formatter, recordFactory));
    }
}
