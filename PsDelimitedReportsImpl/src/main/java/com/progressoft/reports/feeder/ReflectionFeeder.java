package com.progressoft.reports.feeder;

import com.progressoft.reports.Record;
import com.progressoft.reports.RecordsFeeder;

import java.util.*;

public class ReflectionFeeder implements RecordsFeeder {

    private class StructureSegment {
        private final ClassStructure classStructure;
        private final ListIterator<?> iterator;
        private final List<Object> objects = new ArrayList<>();

        StructureSegment(ClassStructure classStructure, Collection<?> objects) {
            this.classStructure = classStructure;
            this.objects.addAll(objects);
            this.iterator = this.objects.listIterator();
        }
    }

    private final Deque<StructureSegment> structureSegments = new LinkedList<>();

    public ReflectionFeeder(ClassStructure classStructure, Collection<?> objects) {
        structureSegments.push(new StructureSegment(classStructure, objects));
    }

    @Override
    public boolean hasNext() {
        if (structureSegments.isEmpty()) return false;
        return structureSegments.peek().iterator.hasNext();
    }

    @Override
    public Record next() {
        if (hasNext()) return getNextRecord(structureSegments.peek());
        throw new NoSuchElementException();
    }

    private Record getNextRecord(final StructureSegment ss) {
        return getClassStructureMatchingRecord(ss.iterator.next(), ss);
    }

    private Record getClassStructureMatchingRecord(final Object rootObject, final StructureSegment ss) {
        removeTopEmptyStructure(ss);
        listenForNestedClassStructures(rootObject, ss);
        return convertObjectToRecord(ss.classStructure, rootObject);
    }

    private void removeTopEmptyStructure(final StructureSegment ss) {
        if (ss.iterator.hasNext())
            return;
        structureSegments.pop();
    }

    private void listenForNestedClassStructures(final Object rootObject, final StructureSegment ss) {
        ss.classStructure.nestedClassStructures((o,s)-> onNestedCollectionFound(o, s), rootObject);
    }

    private Record convertObjectToRecord(final ClassStructure cs, final Object rootObject) {
        return cs.createRecord(rootObject);
    }

    private void onNestedCollectionFound(final Collection<?> nestedObjects, final ClassStructure cs) {
        structureSegments.push(new StructureSegment(cs, nestedObjects));
    }
}
