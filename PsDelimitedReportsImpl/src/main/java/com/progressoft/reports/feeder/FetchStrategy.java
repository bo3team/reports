package com.progressoft.reports.feeder;

import java.util.Collection;
import java.util.Map;

public interface FetchStrategy {

    FetchStrategy collectionFetchStrategy= nestedValue -> (Collection)nestedValue;

    FetchStrategy mapFetchStrategy= nestedValue -> ((Map<?,?>)nestedValue).values();

    Collection getNestedObjectsAsCollection(Object nestedValue);
}
