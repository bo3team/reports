package com.progressoft.reports.feeder.formatter;

import com.progressoft.reports.feeder.ReflectionFieldFactory;

public interface ReflectionFormatter {

    ReflectionFieldFactory FACTORY = new DefaultReflectionFieldFactory();

    String getFormattedValueFromObject(final Object rootObject);

    class FieldNameDoesNotExistInClass extends RuntimeException {
        public FieldNameDoesNotExistInClass(String message, Throwable t) {
            super(message, t);
        }
    }
}
