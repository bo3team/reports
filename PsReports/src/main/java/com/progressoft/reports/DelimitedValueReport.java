package com.progressoft.reports;

public class DelimitedValueReport {

    private final ReportWriter reportWriter;

    private final RecordsFeeder recordsFeeder;

    public DelimitedValueReport(RecordsFeeder g, ReportWriter r) {
        this.reportWriter=r;
        this.recordsFeeder =g;
    }

    public void startWriting() {
        while (recordsFeeder.hasNext())
            writeLine(recordsFeeder.next().toLine());
    }

    private void writeLine(String line) {
        if(recordsFeeder.hasNext()){
            reportWriter.writeLine(line);
            return;
        }
        reportWriter.writeLastLine(line);
    }
}
