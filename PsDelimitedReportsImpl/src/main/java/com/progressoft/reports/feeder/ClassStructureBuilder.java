package com.progressoft.reports.feeder;

import com.progressoft.reports.RecordFactory;

public interface ClassStructureBuilder {

    ClassStructure build();

    ClassStructureBuilder withDelimiter(final String delimiter);

    ClassStructureBuilder withDiscriminator(final String discriminator);

    ClassStructureBuilder withRecordFactory(RecordFactory factory);

    static ClassStructureBuilder make() {
        return new DefaultClassStructureBuilder();
    }
}
